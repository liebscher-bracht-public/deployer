# deployer

Deployer recipes for Liebscher & Bracht applications.

Deployer 2.0 -> PHP ^8.1

Always deploy with rsync.

- Common php applications
- Symfony
- Symfony + Webpack
- Wordpress
- Shopware 5
- Shopware 6
- Laravel 9

# Update composer

    docker run --rm \
    -v ".:/var/www/html" \
    laemmi/php-fpm:8.2 \
    composer update
