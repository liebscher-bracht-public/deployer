<?php

/**
 * gpl-3.0 license
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @package    deployer
 * @author     Michael Lämmlein <michael.laemmlein@liebscher-bracht.com>
 * @copyright  ©2025 Liebscher & Bracht
 * @license    https://www.gnu.org/licenses/gpl-3.0.html
 * @version    1.0.0
 * @since      30.01.25
 */

declare(strict_types=1);

namespace Deployer;

require 'recipe/common.php';
require 'contrib/rsync.php';
require 'contrib/cachetool.php';

// Settings common
set('keep_releases', 3);
set('wordpress_install_dir', 'www');

// Shared files
set('shared_files', [
    '{{wordpress_install_dir}}/wp-config.php'
]);

// Shared dirs
set('shared_dirs', [
    '{{wordpress_install_dir}}/wp-content/uploads',
    '{{wordpress_install_dir}}/wp-content/languages',
]);

// Writable dirs
set('writable_dirs', [
    '{{wordpress_install_dir}}/wp-content/uploads',
    '{{wordpress_install_dir}}/wp-content/languages',
]);

// Settings Rsync
set('rsync_src', '.');
add('rsync', [
    'include' => [
        '/vendor/',
        '/{{wordpress_install_dir}}/',
    ],
    'exclude' => [
        '/*',
        '.DS_Store',
        '.gitignore',
        '.gitkeep',
        '/{{wordpress_install_dir}}/composer.json',
        '/{{wordpress_install_dir}}/license.txt',
        '/{{wordpress_install_dir}}/readme.html',
        '/{{wordpress_install_dir}}/wp-config-sample.php',
    ],
    'flags' => 'rlz'
]);

task('deploy', [
    'deploy:prepare',
    'deploy:publish',
]);

task('deploy:update_code')->disable();

after('deploy:update_code', 'rsync');
after('deploy:symlink', 'cachetool:clear:opcache');
after('deploy:failed', 'deploy:unlock');