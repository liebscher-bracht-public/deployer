<?php

/**
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 * @package    deployer
 * @author     Michael Lämmlein <michael.laemmlein@liebscher-bracht.com>
 * @copyright  ©2020 Liebscher & Bracht
 * @license    http://www.opensource.org/licenses/mit-license.php MIT-License
 * @version    1.0.0
 * @since      24.11.20
 */

declare(strict_types=1);

namespace Deployer;

require 'recipe/common.php';
require 'recipe/rsync.php';
require 'recipe/cachetool.php';
require 'recipe/liebscher-bracht/docker-compose.php';

// Settings
set('ssh_multiplexing', true);
set('bin/cachetool', 'vendor/bin/cachetool');

// Rsync
set('rsync_src', './');
add('rsync', [
    'include' => [
        '/vendor/',
        '/git_lastcommit',
        '/git_version',
    ],
    'exclude' => [
        '/*',
        '.DS_Store',
        '.gitignore',
        '.gitkeep'
    ],
    'flags' => 'rlz'
]);

// Shared files/dirs between deploys
//add('shared_files', []);
//set('shared_files', []);
//add('shared_dirs', []);
//set('shared_dirs', []);

// Writable dirs by web server
//add('writable_dirs', []);
//set('writable_dirs', []);

// Hosts
localhost()
    ->stage('local')
    ->roles('test', 'build')
    ->set('release_path', './');

// Tasks
task('deploy:update_code', [
    'rsync',
]);

task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    'deploy:symlink',
    'cachetool:clear:opcache',
    'deploy:unlock',
    'cleanup',
    'success'
])->desc('Deploy project');

// After
after('deploy:failed', 'deploy:unlock');
