<?php

/**
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 * @package    deployer
 * @author     Michael Lämmlein <michael.laemmlein@liebscher-bracht.com>
 * @copyright  ©2021 Liebscher & Bracht
 * @license    http://www.opensource.org/licenses/mit-license.php MIT-License
 * @version    1.0.0
 * @since      16.02.21
 */

declare(strict_types=1);

namespace Deployer;

require 'recipe/liebscher-bracht/common.php';

set('shopware_public_dir', 'web');
set('shopware_public_path', '{{release_path}}/{{shopware_public_dir}}');
set('shopware_temp_dir', '/tmp/shopware-tmp');
set('shopware_temp_zip_file', '/tmp/shopware.zip');
set('fcgi_sockets', []);
set('shop_host', '');
set('shop_path', '');
set('shop_name', '');
set('shop_title', '');

set('shared_files', [
    '{{shopware_public_dir}}/config.php'
]);

set(
    'shared_dirs', [
    '{{shopware_public_dir}}/media',
    '{{shopware_public_dir}}/files',
    '{{shopware_public_dir}}/var/log',
]);

set(
    'create_shared_dirs', [
    '{{shopware_public_dir}}/media/archive',
    '{{shopware_public_dir}}/media/image',
    '{{shopware_public_dir}}/media/image/thumbnail',
    '{{shopware_public_dir}}/media/music',
    '{{shopware_public_dir}}/media/pdf',
    '{{shopware_public_dir}}/media/unknown',
    '{{shopware_public_dir}}/media/video',
    '{{shopware_public_dir}}/media/temp',
    '{{shopware_public_dir}}/files/documents',
    '{{shopware_public_dir}}/files/downloads'
]);

set(
    'writable_dirs', [
    '{{shopware_public_dir}}/var/cache',
    '{{shopware_public_dir}}/web/cache',
    '{{shopware_public_dir}}/web/sitemap',
    '{{shopware_public_dir}}/recovery',
    '{{shopware_public_dir}}/themes'
]);

set(
    'executable_files', [
    '{{shopware_public_dir}}/bin/console'
]);

// Rsync
add('rsync', [
    'include' => [
        '/{{shopware_public_dir}}/',
    ],
    'exclude' => [
        '/{{shopware_public_dir}}/files/',
        '/{{shopware_public_dir}}/media/',
        '/{{shopware_public_dir}}/var/cache/dev*',
        '/{{shopware_public_dir}}/var/cache/production*',
        '/{{shopware_public_dir}}/var/log/',
        '/{{shopware_public_dir}}/web/cache/',
        '/{{shopware_public_dir}}/web/sitemap/',
        '/{{shopware_public_dir}}/config.php',
        '/{{shopware_public_dir}}/license.txt'
    ]
]);

task(
    'deploy:writable:create_dirs',
    function () {
        foreach (get('writable_dirs') as $dir) {
            run("cd {{release_path}} && mkdir -p $dir");
        }
    }
)->desc('Create required directories, configure via deploy.php');

task(
    'deploy:shared:sub',
    function () {
        $sharedPath = "{{deploy_path}}/shared";
        foreach (get('create_shared_dirs') as $dir) {
            // Create shared dir if it does not exist.
            run("mkdir -p $sharedPath/$dir");
        }
    }
)->desc('Creating shared subdirs');

task(
    'lnb:shopware:download',
    function () {
        run('wget -nc -O {{shopware_temp_zip_file}} {{shopware_download_path}}');
        run('mkdir {{shopware_temp_dir}}');
        run('unzip {{shopware_temp_zip_file}} -d {{shopware_temp_dir}}');
        run('rm {{shopware_temp_zip_file}}');
        run('rm -rf {{shopware_temp_dir}}/recovery');
        run('rsync --ignore-existing --recursive {{shopware_temp_dir}}/ {{shopware_public_path}}/');
        run('rm -rf {{shopware_temp_dir}}');
        run('mkdir -p {{shopware_public_path}}/recovery/install/data');
        run('touch {{shopware_public_path}}/recovery/install/data/install.lock');
    }
)->desc('Download and include Shopware core data into project')->setPrivate();

task(
    'lnb:shopware:update',
    function () {
        if (test('cd {{shopware_public_path}} && {{bin/php}} bin/console k10r:update:needed {{shopware_update_version}} -q')) {
            run('wget -nc -O {{shopware_temp_zip_file}} {{shopware_update_path}}');
            run('mkdir {{shopware_temp_dir}}');
            run('unzip {{shopware_temp_zip_file}} -d {{shopware_temp_dir}}');
            run('rm {{shopware_temp_zip_file}}');
            run('rsync --recursive {{shopware_temp_dir}}/ {{shopware_public_path}}/');
            run('rm -rf {{shopware_temp_dir}}');

            run('cd {{shopware_public_path}} && {{bin/php}} recovery/update/index.php -n');
            run('rm -rf {{shopware_public_path}}/update-assets');
            run('rm -rf {{shopware_public_path}}/recovery');
            run('mkdir -p {{shopware_public_path}}/recovery/install/data');
            run('touch {{shopware_public_path}}/recovery/install/data/install.lock');
        }
    }
)->desc('Check if a Shopware update is needed and execute it');

task(
    'lnb:shopware:vendors',
    function () {
        foreach ([
//                     'k10r/deployment:1.2.0',
                 ] as $dependency) {
            run(
                "cd {{shopware_public_path}} && {{bin/composer}} require --no-scripts --optimize-autoloader --prefer-dist --no-ansi --update-no-dev {$dependency}"
            );
        }
    }
)->desc('Add required composer-based Shopware plugins for deployment')->setPrivate();

task(
    'deploy:vendors',
    function () {
        run('cd {{shopware_public_path}} && {{bin/composer}} {{composer_options}}');
    }
)->desc('Install composer dependencies');

task(
    'lnb:shopware:plugins',
    function () {
        run('cd {{shopware_public_path}} && {{bin/php}} bin/console sw:plugin:refresh -q');
        run('cd {{shopware_public_path}} && {{bin/php}} bin/console sw:plugin:reinstall K10rDeployment');
        run('cd {{shopware_public_path}} && {{bin/php}} bin/console k10r:plugin:deactivate SwagUpdate');

        foreach (get('plugins') as $plugin) {
            run(
                "cd {{shopware_public_path}} && {{bin/php}} bin/console k10r:plugin:install --activate {$plugin}"
            );
        }
    }
)->desc('Install Shopware plugins after deployment. Configure plugins via deploy.php.');

task(
    'lnb:shopware:config',
    function () {
        /*run("cd {{shopware_public_path}} && {{bin/php}} bin/console k10r:snippets:update");*/
    }
)->desc('Update snippets and set Shopware core configuration');

task(
    'lnb:shopware:theme',
    function () {
        run(
            "cd {{shopware_public_path}} && {{bin/php}} bin/console sw:theme:synchronize -q"
        );
        foreach (get('theme_config') as $setting => $value) {
            run(
                "cd {{shopware_public_path}} && {{bin/php}} bin/console k10r:theme:update -q --theme %%THEME_NAME%% --shop %%SHOP_ID%% --setting {$setting} --value {$value}"
            );
        }
    }
)->desc('Configure theme via deploy.php');

task(
    'lnb:shopware:cache',
    function () {
        run('cd {{shopware_public_path}} && {{bin/php}} bin/console sw:cache:clear -q');
        run('cd {{shopware_public_path}} && {{bin/php}} bin/console k10r:theme:compile -q');
        run('cd {{shopware_public_path}} && {{bin/php}} bin/console sw:generate:sitemap -q');
        run('cd {{shopware_public_path}} && {{bin/php}} bin/console lnb:all:cache-clear-direct -q');
        /*        run('cd {{shopware_public_path}} && {{bin/php}} bin/console sw:warm:http:cache -c -q');*/
    }
)->desc('Clear Shopware cache and warm up HTTP cache');

task(
    'lnb:shopware:opcache',
    function () {
        foreach (get('fcgi_sockets') as $socket) {
            run("cd {{release_path}} && if [ -f {{bin/cachetool}} ]; then {{bin/php}} {{bin/cachetool}} opcache:reset --fcgi={$socket}; else true; fi");
        }
    }
)->desc('Clear PHP OPcache');

task(
    'lnb:shopware:apcucache',
    function () {
        foreach (get('fcgi_sockets') as $socket) {
            run("cd {{release_path}} && if [ -f {{bin/cachetool}} ]; then {{bin/php}} {{bin/cachetool}} apcu:cache:clear --fcgi={$socket}; else true; fi");
        }
    }
)->desc('Clear PHP APCUcache');

task(
    'lnb:shopware:staging',
    function () {
        run(
            sprintf('cd {{shopware_public_path}} && {{bin/php}} bin/console k10r:store:update --host %s --path %s --name %s --title %s',
                escapeshellarg(get('shop_host')),
                escapeshellarg(get('shop_path')),
                escapeshellarg(get('shop_name')),
                escapeshellarg(get('shop_title'))
            )
        );
    }
)->desc('Set staging configuration');

/**
 * Main task
 */
task(
    'lnb:shopware:install',
    [
        'lnb:shopware:download',
//        'lnb:shopware:vendors',
    ]
)->desc('Download current Shopware distribution and copy it into deployed directory, install composer-based Shopware plugins');

task(
    'lnb:shopware:configure',
    [
        'lnb:shopware:plugins',
        'lnb:shopware:config',
        'lnb:shopware:theme',
        'lnb:shopware:cache',
    ]
)->desc('Install remaining Shopware plugins, set configuration values and clear cache');

task(
    'deploy:production', [
    'deploy:prepare',
    'lnb:shopware:build',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    'deploy:clear_paths',
    'deploy:symlink',
    'lnb:shopware:update',
    'lnb:shopware:opcache',
    'lnb:shopware:configure',
    'lnb:shopware:apcucache',
    'deploy:unlock',
    'cleanup',
    'success',
])->desc('Deploys a production shopware project');

task(
    'deploy:staging', [
    'deploy:prepare',
    'lnb:shopware:build',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    'deploy:clear_paths',
    'deploy:symlink',
    'lnb:shopware:update',
    'lnb:shopware:opcache',
    'lnb:shopware:configure',
    'lnb:shopware:staging',
    'lnb:shopware:apcucache',
    'deploy:unlock',
    'cleanup',
    'success',
])->desc('Deploys a staging shopware project');

task(
    'deploy:testing', [
//    'lnb:shopware:vendors',
    'lnb:shopware:configure',
    'lnb:shopware:staging',
])->desc('Install and configure a local shopware testing project');

// Before
before('deploy:writable', 'deploy:writable:create_dirs');

// After
after('deploy:shared', 'deploy:shared:sub');
