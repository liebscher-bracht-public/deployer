<?php

/**
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 * @package    deployer
 * @author     Michael Lämmlein <michael.laemmlein@liebscher-bracht.com>
 * @copyright  ©2023 Liebscher & Bracht
 * @license    http://www.opensource.org/licenses/mit-license.php MIT-License
 * @version    1.0.0
 * @since      14.02.23
 */

declare(strict_types=1);

namespace Deployer;

import('recipe/common.php');
import('contrib/rsync.php');
import('contrib/cachetool.php');

// Settings common
set('keep_releases', 3);
set('allow_anonymous_stats', false);
set('default_timeout', 3600);
set('bin/console', '{{bin/php}} {{release_or_current_path}}/bin/console');

// Shared files
set('shared_files', [
    'config/packages/local.yml',
    '.env.local',
]);

// Shared dirs
set('shared_dirs', [
    'custom/plugins',
    'config/jwt',
    'files',
    'var/log',
    'public/media',
    'public/thumbnail',
    'public/sitemap',
]);

// Writable dirs
set('writable_dirs', [
    'custom/plugins',
    'config/jwt',
    'files',
    'public/media',
    'public/sitemap',
    'public/thumbnail',
    'var',
]);

// Settings rsync
set('rsync_src', '.');
add('rsync', [
    'include' => [
        '/git_lastcommit',
        '/git_version',
        '/bin/',
        '/config/',
        '/custom/',
        '/public/',
        '/vendor/',
        '/.env',
        '/composer.json',
        '/composer.lock',
        '/install.lock',
        '/symfony.lock',
    ],
    'exclude' => [
        '/*',
        '.DS_Store',
        '.gitignore',
        '.gitkeep',
        '.htaccess.dist'
    ],
    'flags' => 'rlz'
]);

// This task remotely creates the `install.lock` file on the target server.
task('sw:touch_install_lock', static function () {
    run('cd {{release_path}} && touch install.lock');
});

// This task remotely executes the `bin/build-js.sh` script on the target server.
task('sw:build', static function () {
//    run('cd {{release_path}} && bash ./bin/build-js.sh');
    run('cd {{release_path}} && bash ./bin/lnb/build-js.sh');
});

// This task remotely executes the `theme:compile` console command on the target server.
task('sw:theme:compile', static function () {
    run('cd {{release_path}} && {{bin/console}} theme:dump');
    run('cd {{release_path}} && {{bin/console}} theme:compile');
});

// This task remotely executes the `cache:clear` console command on the target server.
task('sw:cache:clear', static function () {
    run('cd {{release_path}} && {{bin/console}} cache:clear');
});

// This task remotely executes the cache warmup console commands on the target server so that the first user, who
// visits the website doesn't have to wait for the cache to be built up.
task('sw:cache:warmup', static function () {
    run('cd {{release_path}} && {{bin/console}} cache:warmup');
    run('cd {{release_path}} && {{bin/console}} http:cache:warm:up');
});

// This task remotely executes the `database:migrate` console command on the target server.
task('sw:database:migrate', static function () {
    run('cd {{release_path}} && {{bin/console}} database:migrate --all');
});

task('sw:plugin:refresh', function () {
    run('cd {{release_path}} && {{bin/console}} plugin:refresh');
});

task('sw:assets:install', static function () {
    run('cd {{release_path}} && {{bin/console}} assets:install');
});

function getPlugins(): array
{
    $output = explode("\n", run('cd {{release_path}} && {{bin/console}} plugin:list'));

    // Take line over headlines and count "-" to get the size of the cells.
    $lengths = array_filter(array_map('strlen', explode(' ', $output[4])));
    $splitRow = function ($row) use ($lengths) {
        $columns = [];
        foreach ($lengths as $length) {
            $columns[] = trim(substr($row, 0, $length));
            $row = substr($row, $length + 1);
        }
        return $columns;
    };
    $headers = $splitRow($output[5]);
    $splitRowIntoStructure = function ($row) use ($splitRow, $headers) {
        $columns = $splitRow($row);
        return array_combine($headers, $columns);
    };

    // Ignore first seven lines (headline, title, table, ...).
    $rows = array_slice($output, 7, -3);

    $plugins = [];
    foreach ($rows as $row) {
        $pluginInformation = $splitRowIntoStructure($row);
        if (strtolower($pluginInformation['Upgradeable']) === 'no') {
            continue;
        }
        $plugins[] = $pluginInformation;
    }

    return $plugins;
}

// Task to update all installed plugins
task('sw:plugin:update:all', static function () {
    $plugins = getPlugins();
    foreach ($plugins as $plugin) {
        if ($plugin['Installed'] === 'Yes') {
            writeln("<info>Running plugin update for " . $plugin['Plugin'] . "</info>\n");
            run("cd {{release_path}} && {{bin/console}} plugin:update " . $plugin['Plugin']);
        }
    }
});

// Shopware rollback
task('sw:rollback', [
    'rollback',
    'cachetool:clear:opcache',
//    'sw:cache:clear'
]);

// Main task
desc('Deploys your project');
task('deploy', [
    'deploy:prepare',
    'sw:touch_install_lock',
//    'sw:build',
    'sw:database:migrate',
    'sw:plugin:refresh',
    'sw:theme:compile',
    'sw:assets:install',
    'sw:plugin:update:all',
    'sw:cache:clear',
    'deploy:clear_paths',
    'sw:cache:warmup',
    'deploy:publish',
]);

task('deploy:update_code')->disable();

after('deploy:update_code', 'rsync');
after('deploy:symlink', 'cachetool:clear:opcache');
after('deploy:failed', 'deploy:unlock');

// Get configs for plugin and theme for local build
task('sw-build-without-db:get-remote-config', static function () {
    if (!test('[ -d {{current_path}} ]')) {
        return;
    }
    within('{{deploy_path}}/current', function () {
        run('{{bin/console}} bundle:dump');
        download('{{deploy_path}}/current/var/plugins.json', './var/');

        run('{{bin/console}} theme:dump');
        download('{{deploy_path}}/current/files/theme-config', './files/');
    });
});

// Task for build administration
task('sw-build-without-db:build:administration', static function () {
    if (!testLocally('[ -f ./var/plugins.json ]')) {
        return;
    }
    runLocally('CI=1 SHOPWARE_SKIP_BUNDLE_DUMP=1 ./bin/build-administration.sh');
});

// Task for build storefront
task('sw-build-without-db:build:storefront', static function () {
    if (!testLocally('[ -f ./var/plugins.json ]')) {
        return;
    }
    runLocally('CI=1 SHOPWARE_SKIP_BUNDLE_DUMP=1 ./bin/build-storefront.sh');
});

// Task for build script
task('sw-build-without-db:build', [
    'sw-build-without-db:build:administration',
    'sw-build-without-db:build:storefront'
]);

task('sw-build-without-db', [
    'sw-build-without-db:get-remote-config',
    'sw-build-without-db:build',
]);

//before('deploy:update_code', 'sw-build-without-db');
